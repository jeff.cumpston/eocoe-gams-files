$eolcom #

option
         mip = cplex
         optca = 1E-12
         optcr = 0
;

set
         timeset This includes all timesteps or hours - potentially has to be 24 without closure /0*24/
         timeset_min_1 (timeset) This is a subset starting at T = 1 /1*24/
         timeset_min_last (timeset) This is a subset ending at N_T -1 /0*23/
*         timeset_clone (timeset) For double index operations /0*24/

         scenarios This iterates all available price scenarios /1,2/
;

alias(timeset, timeset_clone);

parameter
         N_T size(timeset) /25/
         N_S size(scenarios) /2/
         eta1 conversion efficiency of thermal power produced by the solar field to electric power output (F->E) /0.4/
         eta2 conversion efficiency of the thermal power from the solar field and transferred to the thermal storage (F->S) /0.8/
         eta3 conversion efficiency of the thermal power produced from the thermal storage to electric power output (S->E) /0.35/
         Q_Emax Thermal capacity of the POWER block /125/
         Q_Emin minimum thermal power input to the POWER block /50/
         P_max nominal capacity of the concentrating solar power plant /50/
         Q_Smax energy capacity of the thermal energy STORAGE /700/
         Q_Smin minimum thermal energy level in the thermal energy STORAGE /45/
         R_doDis ramp-down limit for discharging the thermal energy storage [MWel per hour] /35/
         R_upCh ramp-up limit for charging the thermal energy storage /80/
         U0 initial commitment status of the generation unit: 1 if online and 0 otherwise /0/
*N0_on and N0_off: one of them, has to be changed probably, current config means U0=0: plant is off; N0_off =0 : plant only just got switched off
         N0_on amount of hours plant has been online or offline prior to initial timestep: placeholder until further understanding of specification /0/
         N0_off s.a. /0/
         T_Umin minimum up time of the solar power plant /2/
         T_Dmin minimum down time of the solar power plant /2/
         L overwrite /0/
         M overwrite /0/

*initial conditions below as quasi-params, taken from text descirption on p.6 of Dominguez' paper
         Q_s0 equals half of maximum capacity /350/
         Q_fe0 /0/
         Q_se0 /0/
         Q_fs0 /0/
         P0 /0/
         P_fe0 /0/
         P_se0 /0/
*input data below
         Pi(scenarios) / '1' 0.5
                         '2' 0.5 /
*# can this computation be automated, e.g. 1/size(scenarios)
         E(timeset_min_1) solar irradiation expected mean for each hour / '1' 0
                                                                    '2' 0
                                                                    '3' 0
                                                                    '4' 0
                                                                    '5' 0
                                                                    '6' 0
                                                                    '7' 30
                                                                    '8' 90
                                                                    '9' 150
                                                                    '10' 150
                                                                    '11' 150
                                                                    '12' 100
                                                                    '13' 130
                                                                    '14' 150
                                                                    '15' 150
                                                                    '16' 140
                                                                    '17' 150
                                                                    '18' 150
                                                                    '19' 100
                                                                    '20' 60
                                                                    '21' 0
                                                                    '22' 0
                                                                    '23' 0
                                                                    '24' 0 /
         E_hat(timeset_min_1) maximum mean deviation of expected irr.   / '1' 0
                                                                    '2' 0
                                                                    '3' 0
                                                                    '4' 0
                                                                    '5' 0
                                                                    '6' 0
                                                                    '7' 20
                                                                    '8' 50
                                                                    '9' 50
                                                                    '10' 40
                                                                    '11' 30
                                                                    '12' 30
                                                                    '13' 30
                                                                    '14' 30
                                                                    '15' 30
                                                                    '16' 30
                                                                    '17' 30
                                                                    '18' 40
                                                                    '19' 50
                                                                    '20' 50
                                                                    '21' 0
                                                                    '22' 0
                                                                    '23' 0
                                                                    '24' 0 /
* is it possible to autofill GAMMA with ones?
          GAMMA(timeset_min_1) budget of robustness                     / '1' 1
                                                                    '2' 1
                                                                    '3' 1
                                                                    '4' 1
                                                                    '5' 1
                                                                    '6' 1
                                                                    '7' 1
                                                                    '8' 1
                                                                    '9' 1
                                                                    '10' 1
                                                                    '11' 1
                                                                    '12' 1
                                                                    '13' 1
                                                                    '14' 1
                                                                    '15' 1
                                                                    '16' 1
                                                                    '17' 1
                                                                    '18' 1
                                                                    '19' 1
                                                                    '20' 1
                                                                    '21' 1
                                                                    '22' 1
                                                                    '23' 1
                                                                    '24' 1 /
         lambda(timeset_min_1) Price scenario for electricity market    / '1' 52.47
                                                                    '2' 41.25
                                                                    '3' 54.18
                                                                    '4' 35.15
                                                                    '5' 47.43
                                                                    '6' 38.95
                                                                    '7' 26.59
                                                                    '8' 37.57
                                                                    '9' 47.14
                                                                    '10' 56.70
                                                                    '11' 35.88
                                                                    '12' 42.61
                                                                    '13' 41.25
                                                                    '14' 59.83
                                                                    '15' 36.78
                                                                    '16' 53.18
                                                                    '17' 40.42
                                                                    '18' 56.17
                                                                    '19' 47.34
                                                                    '20' 91.99
                                                                    '21' 45.27
                                                                    '22' 64.29
                                                                    '23' 57.68
                                                                    '24' 41.40 /
*lambda: multiple Price scenarios; add later

;
*assignment equation for M and L should be specified here, currently only direct assignment, but poses question how to implement equation that searches min{values}
M = 2; # plant has to stay off two more hours atleast due to initial condition U0 as well as param N0_off
L = 0;

variable
*all variables here are >= 0, thus could be renamed to 'positive variable' to save a lot of trivial constraints/bounds
         obj objective function value
         P(timeset) Power output as well as distinct energy flows below
         P_fe(timeset)
         P_se(timeset)
         Q_s(timeset)
         Q_fe(timeset)
         Q_se(timeset)
         Q_fs(timeset)

*dual variables, connected to varying irradtiation, thus no need for value at T=0
         Z(timeset_min_1) dual variable for constraining under GAMMA # Z('0') is neither required nor could it be bounded
         q(timeset_min_1) dual variable for trivial constraint of E(t) < E+E_hat(t) # q('0') is neither required nor could it be bounded
         y not a vector in timesteps just in scenarios - origins from auxiliary variable of a constraint value
;
*is it possible to ASSIGN 'initial values' or do they have to be enforced to hold via constraint?
*real initial values don't exist due to equation system structure


binary variable
         U(timeset) decision to operate plant during hour T or not

;

equation
         objfunc

*initial conditions
         init_P
         init_P_fe
         init_P_se
         init_Q_s
         init_Q_fe
         init_Q_se
         init_Q_fs

*thermodynamic balances and physical bounds (max, min), name indicates equation number in Dominguez paper
         two(timeset)
         three(timeset)
         five_a(timeset)
         five_b(timeset)
         six_b(timeset)
         seven(timeset)
         eight(timeset)
         nine_a(timeset)
         nine_b(timeset)
         ten_a(timeset)
         ten_b(timeset)
         eleven_a(timeset)
         eleven_b(timeset)
         thirteen that one is hard to understand might cause errors in behaviour
         fourteen(timeset)
         fifteen(timeset)
         sixteen that one is hard to understand might cause errors in behaviour
         seventeen(timeset)
         eighteen(timeset)
         comparisons(timeset) only makes sense to use with mutliple scenarios

*defining >= 0 bounds, name indicates equation number in Dominguez paper
         six_a(timeset)
         twelve_a(timeset)
         twelve_b(timeset)
         twelve_c(timeset)
         twelve_d(timeset)
         twelve_e(timeset) added by DD and no part of original model

*constrains related to dual variables, dual problem expanding on constraint (4) from original paper
         twentytwo(timeset)
         twentythree(timeset)
         twentyfour(timeset) bigger equal zero with condition
         twentyfive bigger equal zero trivial
         twentysix(timeset) bigger equal zero trivial
         twentyseven bound for y from Bertsimas formulation


;

objfunc..        obj =G= -1*sum(timeset_min_1, lambda(timeset_min_1)*P(timeset_min_1));

*assign initial values; technically would be better to enforce dereferring first value instead of '1'
init_P..         P('0') =E= P0;
init_P_fe..      P_fe('1') =E= P_fe0;
init_P_se..      P_se('1') =E= P_se0;
init_Q_s..       Q_s('0') =E= Q_s0;
init_Q_fe..      Q_fe('1') =E= Q_fe0;
init_Q_se..      Q_se('1') =E= Q_se0;
init_Q_fs..      Q_fs('1') =E= Q_fs0;

*bigger equal zero definitions. technically index set is timestep_min_1, because initial conditions already constrain for T=0
six_a(timeset)..         P(timeset) =G= 0;
twelve_a(timeset)..      P_fe(timeset) =G= 0;
twelve_b(timeset)..      P_se(timeset) =G= 0;
twelve_c(timeset)..      Q_fe(timeset) =G= 0;
twelve_d(timeset)..      Q_se(timeset) =G= 0;
twelve_e(timeset)..      Q_fs(timeset) =G= 0;  #added by DD because seems physically true

*thermodynamic balances and physical min/max bounds
two(timeset_min_1)..             P(timeset_min_1) =E= P_fe(timeset_min_1) + P_se(timeset_min_1);
three(timeset_min_1)..           P_fe(timeset_min_1) =E= eta1*Q_fe(timeset_min_1);
five_a(timeset_min_1)..          Q_fe(timeset_min_1) + Q_se(timeset_min_1) =G= Q_Emin*U(timeset_min_1);
five_b(timeset_min_1)..          Q_fe(timeset_min_1) + Q_se(timeset_min_1) =L= Q_Emax*U(timeset_min_1);
six_b(timeset_min_1)..           P(timeset_min_1) =L= P_max;
seven(timeset)$(ord(timeset)>1)..        Q_s(timeset) =E= Q_s(timeset -1) + eta2*Q_fs(timeset) - Q_se(timeset);
eight(timeset_min_1)..           P_se(timeset_min_1) =E= eta3*Q_se(timeset_min_1);
nine_a(timeset_min_1)..          Q_s(timeset_min_1) =G= Q_Smin;
nine_b(timeset_min_1)..          Q_s(timeset_min_1) =L= Q_Smax;
ten_a(timeset)$(ord(timeset)<=N_T)..     P_se(timeset) - P_se(timeset +1) =L= R_doDis;
ten_b(timeset)$(ord(timeset)<=N_T)..     P_se(timeset +1) - P_se(timeset) =L= R_doDis;
eleven_a(timeset)$(ord(timeset)<=N_T)..  eta2*(Q_fs(timeset +1) - Q_fs(timeset)) =L= R_upCh;
eleven_b(timeset)$(ord(timeset)<=N_T)..  eta2*(Q_fs(timeset) - Q_fs(timeset +1)) =L= R_upCh;
*ask Tim for syntax here, if malfunctions; probably bad, because if statement includes indices, which might not be params and thus cannot be presolved?
thirteen(timeset_min_1)$((U0 = 1)and(ord(timeset_min_1) <= L))..                         U(timeset_min_1) =E= 1; #can be summarized into one constraint using sumfunction
fourteen(timeset)$((ord(timeset) -1 >= L +1) and (ord(timeset) <= N_T -T_Umin +1))..        sum(timeset_clone $((ord(timeset_clone) >= ord(timeset)) and (ord(timeset_clone) <= ord(timeset) +T_Umin -1)), U(timeset_clone)) =G= T_Umin*(U(timeset) - U(timeset -1));
fifteen(timeset)$(ord(timeset) >= N_T -T_Umin +2)..                                      sum(timeset_clone $(ord(timeset_clone) >= ord(timeset)), U(timeset_clone) - U(timeset) + U(timeset -1)) =G= 0;
sixteen(timeset_min_1)$((U0 = 0)and(ord(timeset_min_1) <= M))..                          U(timeset_min_1) =E= 0; #can be summarized into one constraint using sumfunction
seventeen(timeset)$((ord(timeset) -1 >= M +1) and (ord(timeset) <= N_T -T_Dmin +1))..       sum(timeset_clone $((ord(timeset_clone) >= ord(timeset)) and (ord(timeset_clone) <= ord(timeset) +T_Dmin -1)), 1 - U(timeset_clone)) =G= T_Dmin*(U(timeset -1) - U(timeset));
eighteen(timeset) $ (ord(timeset) >= N_T -T_Dmin +2)..                                   sum(timeset_clone $(ord(timeset_clone) >= ord(timeset)), 1 - U(timeset_clone) + U(timeset) - U(timeset -1)) =G= 0;
*comparisons(timeset).. only makes sense to use with mutliple scenarios

*dual related constraints
twentytwo(timeset_min_1)..       Q_fe(timeset_min_1) + Q_fs(timeset_min_1) - E(timeset_min_1) + Z(timeset_min_1)*GAMMA(timeset_min_1) + q(timeset_min_1) =L= 0;
twentythree(timeset_min_1)..     Z(timeset_min_1) + q(timeset_min_1) =G= E_hat(timeset_min_1)*y; # $(E_hat(timeset_min_1)) > 0) left out since has to hold anyway
twentyfour(timeset_min_1)..      q(timeset_min_1) =G= 0;   # $(E_hat(timeset_min_1)) > 0) left out since has to hold anyway
twentyfive..                     y =G= 0;
twentysix(timeset_min_1)..       Z(timeset_min_1) =G= 0;
twentyseven..                    y =G= 1; #makes twentyfive redundant


model
         oos optimal offering strategy /objfunc, init_P, init_P_fe, init_P_se, init_Q_s, init_Q_fe, init_Q_se, init_Q_fs,
                                         two, three, five_a, five_b, six_a, six_b, seven, eight, nine_a, nine_b, ten_a, eleven_a, twelve_a, twelve_b, twelve_c, twelve_d, twelve_e, thirteen,
                                         fourteen, fifteen, sixteen, seventeen, eighteen, twentytwo, twentythree, twentyfour, twentysix, twentyseven /

;

solve oos minimizing obj using mip;

display  oos.solvestat, oos.modelstat, oos.objest;

*further displays

